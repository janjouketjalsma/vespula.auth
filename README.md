# README #

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/279a5fc8bb16418c97763f41599a95a1)](https://www.codacy.com/app/jon-elofson/vespula-auth?utm_source=jelofson@bitbucket.org&amp;utm_medium=referral&amp;utm_content=jelofson/vespula.auth&amp;utm_campaign=Badge_Grade)

A simple, flexible authentication class that is easy to set up and understand.

Exceptions may be thrown so you will likely want to catch them using a `try catch` block.

Exceptions should only be thrown due to incorrect configuration or server issues, etc. Exceptions are not thrown when the authentication fails due to bad username and password.

## Table of Contents ##

* [Installation](#markdown-header-installation)
* [Usage](#markdown-header-usage)
* [Adapters](#markdown-header-adapters)
    * [Text Adapter](#markdown-header-text-adapter) (includes Apache htpasswd files and PHP's password_hash())
    * [Sql Adapter](#markdown-header-sql-adapter)
    * [LDAP Adapter](#markdown-header-ldap-adapter) (with and without a known user DN)


## Installation ##

`composer require vespula/auth`


## Usage ##

```
<?php
require '/your/autoloader.php'; // composer for example

$session = new \Vespula\Auth\Session\Session();

// Optionally pass a maximum idle time and a time until the session expires (in seconds)
$max_idle = 1200;
$expire = 3600;
$session = new \Vespula\Auth\Session\Session($max_idle, $expire);

$adapter = new \Vespula\Auth\Adapter\Xyz(...); // Sql, for example

$auth = new \Vespula\Auth\Auth($adapter, $session);

// login condition could be if a $_POST['someval'] is true or whatever
if (login condition) {
    // filter/sanitize these first
    $credentials = [
        'username'=>$_POST['username'],
        'password'=>$_POST['password']
    ];

    // The credentials are passed as an array. This helps 'hide' them if an exception is thrown. Even in development environments.
    $auth->login($credentials);
    if ($auth->isValid()) {
        // Yay....
        echo "Welome " . $auth->getUsername();

        // Get userdata
        $userdata = $auth->getUserdata();
        echo $userdata['fullname'];

        // Shortcut to userdata
        echo $auth->getUserdata('fullname');
    } else {
        // Nay....
        // Wonder why? Any errors?
        $error = $adapter->getError(); // may be no errors. Just bad credentials
        echo "Please try again, if you dare";
    }
}

// Perform a log out. For example, if isset($_GET['logout'] && $_GET['logout'] == '1')
if (logout condition) {
    $auth->logout();
}

// Check if the user is valid (authenticated)
if ($auth->isValid()) {
    // Access some part of site
}

// Has the person been sitting idle too long?
if ($auth->isIdle()) {
    // Sitting around for too long
    // User is automatically logged out and status set to ANON
}

// Did the expire time get reached?
// Note that if the session actually expires, this won't show as being expired.
// This is because there is no status in a non-existent session.
if ($auth->isExpired()) {
    // Sitting around way too long!
    // User is automatically logged out and status set to ANON
}

// Access to user data
$username = $auth->getUsername();

$userdata = $auth->getUserdata(); // varies by adapter

// get a specific key from the $userdata array (assuming it exists in the array)
$email = $auth->getUserdata('email');


```

## Adapters ##

### Text Adapter ###

This adapter can be used to authenticate users based on credentials stored in an `htpasswd` file or from
an array of usernames and passwords. Passwords can be stored as hashes using PHP's `password_hash()` function,
or by Apache's APR1-MD5 algorithm. The APR1-MD5 check is made possible thanks to the
APR1-MD5 package by Jeremy Ebler [https://github.com/whitehat101/apr1-md5].

Apache htpasswd files can easily be created using Apache's `htpasswd` command. For example,
to create a password file and add a user, you would issue the following:

```
$ htpasswd -c /path/to/.htpasswd juser
```

To add another user to the same file, issue the following noting the omission of the -c switch:

```
$ htpasswd /path/to/.htpasswd otheruser
```

By default, Apache will hash the password using a custom md5 hashing algorithm. You can also use the `bcrypt` algorithm
by using the -B option.

```
$ htpasswd -B /path/to/.htpasswd jdoe
```

The resulting password file may look something like this:

```
juser:$apr1$Y.cxiqTt$4SPe4SHcbX6yzrIMlYtJJ0
otheruser:$apr1$kn0MShZv$RCvYu/wezJ2nAA9mH6BNE0
jdoe:$2y$05$CpZSkygbkm5HHoYYF2lnx..qB4dGywQpIT5GsqWeO26mkPnNzzu8G
```

Now we can create the Text adapter passing the full path of the file to the constructor.


```
<?php
$session = new \Vespula\Auth\Session\Session();
$passwordfile = '/path/to/.htpasswd';
$adapter = new \Vespula\Auth\Adapter\Text($passwordfile);

// This data is returned by the getUserdata() method
$adapter->setUserdata('juser', [
    'fullname'=>'Joe User',
    'email'=>'juser@vespula.com'
]);

$auth = new \Vespula\Auth\Auth($adapter, $session);

if ('login button pushed logic') {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'password', FILTER_DEFAULT);
    $credentials = [
        'username'=>$username,
        'password'=>$password
    ];
    $auth->login($credentials);
    if ($auth->isValid()) {
        // display message, redirect, etc.
        $userdata = $auth->getUserdata();
        echo 'Hello, ' . $auth->getUsername();
        echo 'Your fullname is ' . $userdata['fullname'];
        // Or...
        echo 'Your fullname is ' . $auth->getUserdata('fullname');

    } else {
        // no luck, bad password or username
    }
}

if ('logout link clicked') {
    $auth->logout();
    // bye bye
    // $auth->isAnon() should return true
}
```

You may also pass an array of users and passwords to the Text adapter constructor. Passwords may be
hashed using Apache's APR1-MD5 algorithm, Apache's bcrypt algorithm, or PHP's `password_hash()` function. For example:

```
<?php
$session = new \Vespula\Auth\Session\Session();


$passwords = [
    'juser'=>'$apr1$Y.cxiqTt$4SPe4SHcbX6yzrIMlYtJJ0',
    'jsmith'=>'$2y$10$XNbpIsW6zI1QIR1tdCXr5.7DuE8Jpfd8yAdQ8czcjDbUrykCBDYXm'
];

$adapter = new \Vespula\Auth\Adapter\Text($passwords);

// This data is returned by the getUserdata() method
$adapter->setUserdata('juser', [
    'fullname'=>'Joe User',
    'email'=>'juser@vespula.com'
]);

$auth = new \Vespula\Auth\Auth($adapter, $session);

```

#### Loading User Data ####

You may want to load user data, such as email, full name, etc., into the auth object. This can be done in one of two ways.

**Load data as an array**

```php
<?php

$session = new \Vespula\Auth\Session\Session();

$passwords = [
    'juser'=>'$apr1$Y.cxiqTt$4SPe4SHcbX6yzrIMlYtJJ0',
    'jsmith'=>'$2y$10$XNbpIsW6zI1QIR1tdCXr5.7DuE8Jpfd8yAdQ8czcjDbUrykCBDYXm'
];

$adapter = new \Vespula\Auth\Adapter\Text($passwords);

$userdata = [
    'juser'=>[
        'fullname'=>'Joe User',
        'email'=>'juser@example.com'
    ],
    'jsmith'=>[
        'fullname'=>'John Smith',
        'email'=>'jsmith@example.com'
    ],
];

$adapter->loadUserData($userdata);

$auth = new \Vespula\Auth\Auth($adapter, $session);

// Now you can get userdata by username

$info = $auth->getUserdata();

// Or get a specific key

$email = $auth->getUserdata('email');

```

**Set data for individual username**

```php
<?php

$session = new \Vespula\Auth\Session\Session();

$passwords = [
    'juser'=>'$apr1$Y.cxiqTt$4SPe4SHcbX6yzrIMlYtJJ0',
    'jsmith'=>'$2y$10$XNbpIsW6zI1QIR1tdCXr5.7DuE8Jpfd8yAdQ8czcjDbUrykCBDYXm'
];

$adapter = new \Vespula\Auth\Adapter\Text($passwords);

$adapter->setUserdata('juser', [
    'fullname'=>'Joe User',
    'email'=>'juser@example.com'
]);


$auth = new \Vespula\Auth\Auth($adapter, $session);

// Now you can get userdata by username

$info = $auth->getUserdata();

// Or get a specific key

$email = $auth->getUserdata('email');

```

### Sql Adapter ###

Authenticate against a database table where users' passwords are stored using PHP's bcrypt hashing and the PASSWORD_DEFAULT algorithm.
Under the hood, it uses `password_verify()`.

```
// user table //
+----------+-------------+------------+---------------------+
| username | fullname    | email      | bcryptpass          |
|----------+-------------+------------+---------------------|
| juser    | Joe User    | juser@...  | $2y$.............   |
+----------+-------------+------------+---------------------+

```

```
<?php
$session = new \Vespula\Auth\Session\Session();
$dsn = 'mysql:dbname=mydatabase;host=localhost';
$pdo = new \Pdo($dsn, 'dbuser', '********');

// $cols array must have a 'username' and 'password' element. You can use an alias if needed. See below.
// This data (except username and password) will populate the `getUserdata()` array
$cols = [
    'username',
    'bcryptpass'=>'password', // alias
    'fullname'=>'full_name' // alias
    'email'
];
$from = 'user';
$where = 'active=1'; // optional

$adapter = new \Vespula\Auth\Adapter\Sql($pdo, $from, $cols, $where);
$auth = new \Vespula\Auth\Auth($adapter, $session);

if ('login button pushed logic') {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'password', FILTER_DEFAULT);
    $credentials = [
        'username'=>$username,
        'password'=>$password
    ];
    $auth->login($credentials);
    if ($auth->isValid()) {
        // display message, redirect, etc.
        $userdata = $auth->getUserdata();
        echo 'Hello, ' . $auth->getUsername();
        echo 'Your fullname is ' . $userdata['full_name']; // note the use of the alias (not fullname)
        // Or...
        echo 'Your fullname is ' . $auth->getUserdata('fullname');
    } else {
        // no luck, bad password or username
    }
}

if ('logout link clicked') {
    $auth->logout();
    // bye bye
    // $auth->isAnon() should return true
}
```

### Ldap Adapter ###

This adapter authenticates against active directory using LDAP. If you know the DN format, you and pass that to the constructor. If you don't know it, then you can pass bind options to find the user's DN.

**Example 1: Known DN format**

```
<?php

$session = new \Vespula\Auth\Session\Session();
$uri = 'ldap.mycompany.org';
$dn = 'cn=%s,OU=Users,OU=MyCompany,OU=Edmonton,OU=Alberta'; //%s replaced by username internally
$ldap_options = [
    LDAP_OPT_PROTOCOL_VERSION=>3,
    LDAP_OPT_REFERRALS=>0
];

// These attributes populate the `getUserdata()` array.
// Use array keys for aliases, values for the LDAP attribute name.
// Note: Be sure to define keys for all attributes or none of them. Otherwise there will be integer indexed attribute values.
$attributes = [
    'email' => 'email',
    'firstName' => 'givenname',
    'lastName' => 'sn'
];

$adapter = new \Vespula\Auth\Adapter\Ldap($uri, $dn, null, $ldap_options, $attributes);
$auth = new \Vespula\Auth\Auth($adapter, $session);

if ('login button pushed logic') {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'password', FILTER_DEFAULT);
    $credentials = [
        'username'=>$username,
        'password'=>$password
    ];
    $auth->login($credentials);
    if ($auth->isValid()) {
        // display message, redirect, etc.
        $userdata = $auth->getUserdata();
        echo 'Hello, ' . $auth->getUsername();
        echo 'Your fullname is ' . $userdata['givenname'];
        // Or...
        echo 'Your fullname is ' . $auth->getUserdata('givenname');
    } else {
        // no luck, bad password or username
    }
}

if ('logout link clicked') {
    $auth->logout();
    // bye bye
    // $auth->isAnon() should return true
}
```

**Example 2: Unknown DN format**

```
<?php

$session = new \Vespula\Auth\Session\Session();
$uri = 'ldap.mycompany.org';

// Specify bind options to look up the user's dn
$bind_options = [
    'basedn'=>'OU=MyCompany,OU=Edmonton,OU=Alberta',
    'binddn'=>'cn=specialuser,OU=MyCompany,OU=Edmonton,OU=Alberta',
    'bindpw'=>'********',
    'filter'=>'cn=%s' // How to find the particular user in the base dn
];


$ldap_options = [
    LDAP_OPT_PROTOCOL_VERSION=>3,
    LDAP_OPT_REFERRALS=>0
];

// Example without aliases
$attributes = [
    'email',
    'givenname'
];

$adapter = new \Vespula\Auth\Adapter\Ldap($uri, null, $bind_options, $ldap_options, $attributes);
$auth = new \Vespula\Auth\Auth($adapter, $session);

if ('login button pushed logic') {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'password', FILTER_DEFAULT);
    $credentials = [
        'username'=>$username,
        'password'=>$password
    ];
    $auth->login($credentials);
    if ($auth->isValid()) {
        // display message, redirect, etc.
        $userdata = $auth->getUserdata();
        echo 'Hello, ' . $auth->getUsername();
        echo 'Your fullname is ' . $userdata['givenname'];
        // Or...
        echo 'Your fullname is ' . $auth->getUserdata('givenname');
    } else {
        // no luck, bad password or username
    }
}

if ('logout link clicked') {
    $auth->logout();
    // bye bye
    // $auth->isAnon() should return true
}
```

**Modifying Escape Characters**

The LDAP adapter automatically escapes the username using PHP's `addcslashes()`. The default escape characters are
`\\&!|=<>,+-"\';()`. So, for example, if you had a username that was `my-username`, the adapter would escape the -
which would result in `my\-username`. This likely would fail. So, if you need to modify the escape characters, you can use
the `$apapter->setEscapeChars()` method.

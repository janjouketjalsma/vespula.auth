<?php
namespace Vespula\Auth\Adapter;

use WhiteHat101\Crypt\APR1_MD5;

/**
 * This class is for authenticating users by simple text data. This is
 * for testing purposes only and should not be used in production.
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 */
class Text implements AdapterInterface
{

    /**
     * Error when no user found in the passwords array
     *
     * @var string
     */
    const ERROR_NO_USER = 'ERROR_NO_USER';

   /**
    *
    * @see setPassword()
    * @var array Hashed passwords keyed on username
    */
    protected $passwords = [];

    /**
     *
     * @see setUserData()
     * @var array of userdata
     */
    protected $userdata = [];

    /**
     * @var string Debugging info
     */
    protected $error;

    public function __construct($data)
    {
        if (is_string($data)) {
            $this->passwords = $this->loadFromFile($data);
            return;
        }

        if (is_array($data)) {
            $this->passwords = $data;
            return;
        }

        throw new \Vespula\Auth\Exception('Invalid data passed. Must be a filename or array of users');
    }

    protected function loadFromFile($filename)
    {
        if (!file_exists($filename)) {
            throw new \Vespula\Auth\Exception('File not found ' . $filename);
        }
        $lines = file($filename, FILE_IGNORE_NEW_LINES);

        $passwords = [];
        foreach ($lines as $line) {
            $data = explode(":", $line);
            $passwords[$data[0]] = $data[1];
        }

        return $passwords;

    }

    /**
     * {@inheritDoc}
     * @see \Vespula\Auth\Adapter\AdapterInterface::authenticate()
     */
    public function authenticate(array $credentials)
    {
        // explicit vs `extract`
        $username = $credentials['username'];
        $password = $credentials['password'];

        if (! isset($this->passwords[$username])) {
            $this->error = Text::ERROR_NO_USER;
            return false;
        }

        if (substr($this->passwords[$username], 0, 4) == '$apr') {
            return $this->apr1md5($password, $this->passwords[$username]);
        }

        if (substr($this->passwords[$username], 0, 4) == '$2y$') {
            return $this->php_verify($password, $this->passwords[$username]);
        }

        return false;
    }

    protected function apr1md5($password, $hash)
    {
        return APR1_MD5::check($password, $hash);
    }

    protected function php_verify($password, $hash)
    {
        return password_verify($password, $hash);
    }

    /**
     * Set user-specific data
     *
     * @param string $username
     * @param array $data
     */
    public function setUserData($username, array $data)
    {
        $this->userdata[$username] = $data;
    }
    
    /**
     * Loads user data from an array. Array must have keys for users and an array 
     * of data for each user.
     * 
     * $data = [
     *     'juser'=>[
     *         'fullname'=>'Joe User',
     *         'email'=>'juser@awesome.com'
     *     ],
     * ];
     * 
     * @param array $data
     */
    public function loadUserData(array $data)
    {
        foreach ($data as $username=>$userdata) {
            $this->setUserData($username, $userdata);
        }
    }

    /**
     *
     * {@inheritDoc}
     * @see \Vespula\Auth\Adapter\AdapterInterface::lookupUserData()
     */
    public function lookupUserData($username)
    {
        return array_key_exists($username, $this->userdata) ? $this->userdata[$username] : [];
    }

    /**
     *
     * {@inheritDoc}
     * @see \Vespula\Auth\Adapter\AdapterInterface::getError()
     */
    public function getError()
    {
        return $this->error;
    }


}

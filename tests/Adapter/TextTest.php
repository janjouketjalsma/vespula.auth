<?php
namespace Vespula\Auth\Adapter;
use WhiteHat101\Crypt\APR1_MD5;
class TextTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructFile()
    {
        $filename = __DIR__ . '/htpasswords.txt';

        $adapter = new Text($filename);

        $this->assertTrue($adapter->authenticate([
            'username'=>'juser',
            'password'=>'foobar'
        ]));

        $this->assertTrue($adapter->authenticate([
            'username'=>'jsmith',
            'password'=>'PassW0rd'
        ]));

        $this->assertTrue($adapter->authenticate([
            'username'=>'jdoe',
            'password'=>'secret'
        ]));
    }

    public function testConstructArray()
    {
        $users = [
            'juser'=>'$apr1$Y.cxiqTt$4SPe4SHcbX6yzrIMlYtJJ0',
            'jsmith'=>APR1_MD5::hash('PassW0rd'),
            'jdoe'=>'$2y$10$XNbpIsW6zI1QIR1tdCXr5.7DuE8Jpfd8yAdQ8czcjDbUrykCBDYXm'
        ];

        $adapter = new Text($users);

        $this->assertTrue($adapter->authenticate([
            'username'=>'juser',
            'password'=>'foobar'
        ]));

        $this->assertTrue($adapter->authenticate([
            'username'=>'jsmith',
            'password'=>'PassW0rd'
        ]));

        $this->assertTrue($adapter->authenticate([
            'username'=>'jdoe',
            'password'=>'secret'
        ]));
    }

    /**
     * @expectedException \Vespula\Auth\Exception
     */
    public function testConstructExceptionNull()
    {
        $adapter = new Text(null);
    }

    /**
     * @expectedException \Vespula\Auth\Exception
     */
    public function testConstructExceptionInt()
    {
        $adapter = new Text(3);
    }

    /**
     * @expectedException \Vespula\Auth\Exception
     */
    public function testConstructExceptionBadFile()
    {
        $adapter = new Text('blah.txt');
    }

    public function testUserData()
    {
        $users = [
            'juser'=>'$apr1$Y.cxiqTt$4SPe4SHcbX6yzrIMlYtJJ0',
            'jsmith'=>'$apr1$kn0MShZv$RCvYu/wezJ2nAA9mH6BNE0'
        ];

        $adapter = new Text($users);

        $adapter->setUserData('juser', [
            'fullname'=>'Joe User',
            'email'=>'juser@example.com'
        ]);

        $expected = [
            'fullname'=>'Joe User',
            'email'=>'juser@example.com'
        ];

        $this->assertEquals($expected, $adapter->lookupUserData('juser'));
    }

    public function testNoUser()
    {
        $users = [
            'juser'=>'$apr1$Y.cxiqTt$4SPe4SHcbX6yzrIMlYtJJ0',
            'jsmith'=>'$apr1$kn0MShZv$RCvYu/wezJ2nAA9mH6BNE0'
        ];

        $adapter = new Text($users);

        $this->assertFalse($adapter->authenticate([
            'username'=>'nobody',
            'password'=>'*******'
        ]));

        $this->assertEquals(Text::ERROR_NO_USER, $adapter->getError());
    }

    public function testBadPasswd()
    {
        $users = [
            'juser'=>'$apr1$Y.cxiqTt$4SPe4SHcbX6yzrIMlYtJJ0',
            'jsmith'=>'$apr1$kn0MShZv$RCvYu/wezJ2nAA9mH6BNE0'
        ];

        $adapter = new Text($users);

        $this->assertFalse($adapter->authenticate([
            'username'=>'juser',
            'password'=>'*******'
        ]));
    }

    public function testSuccess()
    {
        $users = [
            'juser'=>APR1_MD5::hash('foobar'),
        ];

        $adapter = new Text($users);

        $this->assertTrue($adapter->authenticate([
            'username'=>'juser',
            'password'=>'foobar'
        ]));

    }
    
    public function testLoadUserData()
    {
        $users = [
            'juser'=>'$apr1$Y.cxiqTt$4SPe4SHcbX6yzrIMlYtJJ0',
            'jsmith'=>'$apr1$kn0MShZv$RCvYu/wezJ2nAA9mH6BNE0'
        ];

        $adapter = new Text($users);

        $data = [
            'juser'=>[
                'fullname'=>'Joe User',
                'email'=>'juser@example.com'
            ],
            'jsmith'=>[
                'fullname'=>'John Smith',
                'email'=>'jsmith@example.com'
            ]
        ];
        $adapter->loadUserData($data);

        $expectedJuser = [
            'fullname'=>'Joe User',
            'email'=>'juser@example.com'
        ];

        $this->assertEquals($expectedJuser, $adapter->lookupUserData('juser'));
        
        $expectedJsmith = [
            'fullname'=>'John Smith',
            'email'=>'jsmith@example.com'
        ];

        $this->assertEquals($expectedJsmith, $adapter->lookupUserData('jsmith'));
    }
}
